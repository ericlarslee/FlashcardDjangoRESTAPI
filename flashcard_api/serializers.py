from rest_framework import serializers
from .models import Flashcard, Collection


class FlashcardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Flashcard
        fields = ['id', 'question', 'answer', 'collection']


class ColectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Collection
        fields = ['id', 'name']